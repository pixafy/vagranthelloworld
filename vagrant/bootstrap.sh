#!/bin/sh
#
# vagrant/bootstrap.sh
# Responsible for provisioning the VM. Installs all necessary applications, copies over configuration,
# seeds the database, and configures server
#
# @package		Vagrant
# @author		Thomas Lackemann <tlackemann@pixafy.com>
# @copyright	2014 Pixafy
#
# @created		2014/05/13
#
MAGENTO_MYSQL="vagrant_local" # The name of the Magento DB
SITE_DIR="/data/www/local" # The path to the website on the server (default: /data/www/local)
MAGENTO_DIR="current"
DATABAG_DIR="/vagrant/vagrant/databags"

# APPLICATIONS
echo "Adding essential development tools ..."
yum install -y php-dom # for Magento
yum install -y php-mcrypt # for Magento
yum install -y php-gd # for Magento
yum install -y php-mysql # for Magento
yum install -y php-soap # for Magento
yum install -y git
yum install -y vim

# PHPMYADMIN
mysqladmin -u root password root
yum install -y phpmyadmin

# APACHE SYMLINK
echo "Symlinking site ..."
mkdir -p $SITE_DIR
ln -s /vagrant $SITE_DIR/$MAGENTO_DIR
sudo ln -s /usr/share/phpMyAdmin $SITE_DIR/dbadmin #symlink phpMyAdmin

# LOCAL.XML
echo "Configuring website ..."
rm -f $SITE_DIR/$MAGENTO_DIR/app/etc/local.xml; ln -s $DATABAG_DIR/magento/local.xml $SITE_DIR/$MAGENTO_DIR/app/etc/local.xml # Magento local.xml

# DATABASE
echo "Creating Magento database"
mysql -u root --password=root --execute="drop database if exists $MAGENTO_MYSQL; create database $MAGENTO_MYSQL"
echo "Seeding database ..."
mysql -u root --password=root -f $MAGENTO_MYSQL < $DATABAG_DIR/magento/latest.sql;

echo "Configuring nginx ..."
rm -rf /etc/nginx/sites-enabled/*
rm -rf /etc/php-fpm.d/www.conf
cp $DATABAG_DIR/nginx/sites/* /etc/nginx/sites-enabled/
cp $DATABAG_DIR/php-fpm/* /etc/php-fpm.d/

# Disabling the development firewall
service iptables stop
chkconfig iptables off
chkconfig iptables --del

# Reload services
service nginx restart
service php-fpm restart

echo "Server provisioning complete!"