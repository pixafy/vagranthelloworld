# Vagrant Hello World

Vagrant is a powerful tool that allows developers to seamlessly install, maintain, and upgrade existing web applications.

[http://www.pixafy.com/blog/vagrant-or-how-i-learned-to-stop-worrying](http://www.pixafy.com/blog/vagrant-or-how-i-learned-to-stop-worrying)


Author: Thomas Lackemann <tlackemann@pixafy.com>

Copyright: 2014 Pixafy

## Installation

Navigate to the project folder and type ``vagrant up``. The server will start provisioning (~10 minutes)

## Developing

Feel free to play with the demo, make changes, install themes, write modules, anything really. If you want to share your work with the community via this demo then here's how to do so.

1. Create a new branch - Call it feature/"your-feature-name-here"
2. Write code and commit as needed
3. When finished, push your branch to this repository
4. Issue a pull request for your feature --> master
5. Wait for approval
6. Celebrate your new found success and fame

The fame and success part might be stretch but it should be very straightfoward to create content for this demo. Sharing your code allows other developers to maintain and actively develop this project with no need to understand the underlying architecture (which is great if your primary focus is frontend development.)

We encourage you to play, break, and create anything you'd like with this.

Cheers!