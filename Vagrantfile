# -*- mode: ruby -*-
# vi: set ft=ruby :
#
# Vagrantfile
# Responsible for manging and configuring the VM
#
# @package    Vagrant
# @author     Thomas Lackemann <tlackemann@pixafy.com>
# @copyright  2014 Pixafy
#
# @created    2014/05/13
#

VAGRANTFILE_API_VERSION = "2"
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  #=========================================================================================#
  #===================================== CONFIGURATION =====================================#
  #=========================================================================================#

  # HOSTNAME - Change to your local development URL (e.g. topps.local)
  config.vm.hostname = "vagrant.local"

  # INTERNAL NETWORK - The IP to use when automatically configuring your hosts file
  config.vm.network :private_network, ip: "192.168.100.100"

  # SUBDOMAINS - Uncomment the line below to configure any necessary subdomains (e.g. entertainment.topps.local)
  # config.hostsupdater.aliases = ["herculo.pixafy.local"]

  #=========================================================================================#
  #============================== DO NOT EDIT BELOW THIS LINE ==============================#
  #=========================================================================================#

  # VM CONFIGURATION - DO NOT EDIT - Defines the attributes of the VM
  config.vm.box = "base/centos64"
  # Taken from http://www.vagrantbox.es
  config.vm.box_url = "http://developer.nrel.gov/downloads/vagrant-boxes/CentOS-6.4-x86_64-v20131103.box"

  config.vm.provider :virtualbox do |vb|
    vb.customize ["modifyvm", :id, "--memory", "4096"]
  end

  # CHEF - DO NOT EDIT - Runs the chef provisioner and installs necessary packages on the VM
  config.vm.provision :chef_solo do |chef|
    # chef.log_level = :debug
    chef.cookbooks_path = ["vagrant/cookbooks"]
  
    chef.add_recipe "yum"
    chef.add_recipe "yum-epel"
    chef.add_recipe "ohai"
    chef.add_recipe "nginx::source"
    chef.add_recipe "php-fpm"
    chef.add_recipe "mysql::server"
    
    chef.json = {
      :nginx => {
        :version => "1.5.13",
        :dir => "/etc/nginx",
        :log_dir => "/var/log/nginx",
        :binary => "/opt/nginx-1.5.13/sbin",
        :init_style => "init",
        :listen_port => 80,
        :source => {
          
        }
      }
    }
  end

  # Run the auto-setup script
  config.vm.provision :shell, :path => "vagrant/bootstrap.sh"
end
